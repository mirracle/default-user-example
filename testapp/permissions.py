from django.contrib.auth.models import AnonymousUser
from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsPostOwner(BasePermission):
    def has_permission(self, request, view):
        print('11111')
        print(type(request.user))
        print(request.method)
        if request.method == 'GET':
            return True
        return type(request.user) != AnonymousUser

    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return True
        return obj.author == request.user
