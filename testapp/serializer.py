from rest_framework import serializers
from testapp.models import Post


class PostSerializer(serializers.ModelSerializer):
    author = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Post
        fields = '__all__'
        # exclude = ('author',)

    # def create(self, validated_data):
    #     author = self.context.get('request').user
    #     instance = Post.objects.create(author=author, **validated_data)
    #     return instance
